# Scripts d'alignements des données de CESAR

Des scripts très incomplets `:'(` dans des jupyter notebook (`.ypnb`)


```
└── alignements
    ├── copies_bnf
    │   ├── align_copies.ipynb
    │   ├── all_bnf_copies.csv
    │   ├── ambiguous_manu-matches.csv
    │   ├── ambiguous_matches.csv
    │   ├── cesar_copy.csv
    │   ├── copies_json_aligned.csv
    │   ├── copy_title-dates-editor.csv
    │   ├── direct_matches.csv
    │   ├── no_bnf_answer.csv
    │   ├── no_bnf_answer_manu-match.csv
    │   ├── panda-merge-copies.ipynb
    │   └── reconciliated_matches.csv
    ├── locations
    │   ├── location_cities_regions_countries.csv
    │   ├── location_geonames_match.ipynb
    │   ├── matches_locations.csv
    │   ├── no_match_locations.csv
    │   └── unsure_locations.csv
    ├── persons
    │   ├── cesar_wikidata_persons.sparql
    │   └── cesar_wikidata_persons.tsv

```

<h2>copies_bnf</h2>

Alignement des exemplaires à partir des cotes de la BNF renseignées dans les données de CESAR.  

- Input données de CESAR : `copy_title-dates-editor.csv`
- Produit
  - `direct_matches.csv`
  - `no_bnf_answer.csv`
  - `reconciliated_matches.csv`
  - `ambiguous_matches.csv`
- Retravaillé à la main dans les fichiers : 
  - `ambiguous_manu-matches.csv ` 
  - `no_bnf_answer_manu-match.csv`
- Fusionné dans `all_bnf_copies.csv`
- **`panda-merge-copies.ipynb`** : reformatte les alignements en JSON et le réinsère dans la table des copies : fusionne `all_bnf_copies.csv` et `cesar_copy.csv` vers `copies_json_aligned.csv`. 
- **Mais désormais, les notices de la BNF / Numérisations Gallica sont alignées avec la éditions (publication) et non plus les copies : `cesar-symfony/application/data/publication_bnf_align_from_copies.csv`**  (reformatté moitié SQL / moitié REGEX en chercher-remplacer). 

<h2>locations</h2>

`location_geonames_match.ipynb` prend `location_cities_regions_countries.csv` comme input et tente d'aligner les entités en requêtant l'API de Geoanmes. Il produit : 
- `matches_locations.csv` (reste de nombreuses ambiguïtés à vérifier manuellement)
- `no_match_locations.csv`
- `unsure_locations.csv`

<h2>persons</h2> 

Un wiki bot, et des validateurs humains se sont occupés (avant 2020) d'aligner 1511 personnes de la base CESAR avec des entités de wikidata. 
la requête `cesar_wikidata_persons.sparql` à lancer (par copier coller)sur https://query.wikidata.org/récupère toutes les entités personnes de wikidata dotées d'une propriété  wdt:P2340 : identifiant César (P2340) : identifiant de la personne dans la base de données César consacrée au théâtre français des XVIIe et XVIIIe siècles. Cette propriété pointe vers un `originalId` (6 chiffres), similaire à celui de la base CESAR originale : facile de réintégrer les URI wikidata ramenées dans `cesar_wikidata_persons.tsv` dans le person.csv importé dans CESAR. 
